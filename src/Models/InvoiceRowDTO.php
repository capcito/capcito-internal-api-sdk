<?php

namespace Capcito\InternalApiSdk\Models;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class InvoiceRowDTO extends DataTransferObject
{
    public ?int $id;
    public float $amount;
    public ?string $name;
    public int $quantity;
    public ?string $unit;
    public float $vatRate;
}
