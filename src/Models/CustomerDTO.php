<?php

namespace Capcito\InternalApiSdk\Models;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class CustomerDTO extends DataTransferObject
{
	public ?int $id;
	public ?string $externalId;
	public ?string $name;
	public string $identityNumber;
}
