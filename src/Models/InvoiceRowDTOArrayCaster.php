<?php

namespace Capcito\InternalApiSdk\Models;

use Capcito\InternalApiSdk\Models\InvoiceRowDTO;
use Exception;
use Spatie\DataTransferObject\Caster;

class InvoiceRowDTOArrayCaster implements Caster
{
    public function cast(mixed $value): array
    {
        if (! is_array($value)) {
            throw new Exception("Can only cast arrays to InvoiceRowDTO");
        }

        return array_map(
            fn (array $data) => new InvoiceRowDTO(...$data),
            $value
        );
    }
}
