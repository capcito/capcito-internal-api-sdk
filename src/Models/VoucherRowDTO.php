<?php

namespace Capcito\InternalApiSdk\Models;

use Carbon\Carbon;
use Capcito\InternalApiSdk\Models\PaymentDTO;
use Capcito\InternalApiSdk\Models\CustomerDTO;
use Capcito\InternalApiSdk\Models\InvoiceRowDTO;
use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;
use Capcito\InternalApiSdk\Casters\ToCarbonCaster;
use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Attributes\DefaultCast;
use Capcito\InternalApiSdk\Models\PaymentDTOArrayCaster;
use Capcito\InternalApiSdk\Models\TaxReductionDetailDTO;
use Capcito\InternalApiSdk\Models\InvoiceRowDTOArrayCaster;

#[Strict]
#[DefaultCast(Carbon::class, ToCarbonCaster::class)]
class VoucherRowDTO extends DataTransferObject
{
    public ?int $id;
    public ?int $voucherHeadId;
    public int $credit;
    public int $debit;
    public int $account;
    public int $costCenter;
    public ?Carbon $createdAt;
    public ?Carbon $updatedAt;
}
