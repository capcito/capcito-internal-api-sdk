<?php

namespace Capcito\InternalApiSdk\Models;

use Capcito\InternalApiSdk\Models\VoucherRowDTO;
use Exception;
use Spatie\DataTransferObject\Caster;

class VoucherRowDTOArrayCaster implements Caster
{
    public function cast(mixed $value): array
    {
        if (! is_array($value)) {
            throw new Exception("Can only cast arrays to InvoiceRowDTO");
        }

        return array_map(
            fn (array $data) => new VoucherRowDTO(...$data),
            $value
        );
    }
}
