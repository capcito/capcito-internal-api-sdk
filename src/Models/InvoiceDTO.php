<?php

namespace Capcito\InternalApiSdk\Models;

use Carbon\Carbon;
use Capcito\InternalApiSdk\Models\PaymentDTO;
use Capcito\InternalApiSdk\Models\CustomerDTO;
use Capcito\InternalApiSdk\Models\InvoiceRowDTO;
use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;
use Capcito\InternalApiSdk\Casters\ToCarbonCaster;
use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Attributes\DefaultCast;
use Capcito\InternalApiSdk\Models\PaymentDTOArrayCaster;
use Capcito\InternalApiSdk\Models\TaxReductionDetailDTO;
use Capcito\InternalApiSdk\Models\InvoiceRowDTOArrayCaster;

#[Strict]
#[DefaultCast(Carbon::class, ToCarbonCaster::class)]
class InvoiceDTO extends DataTransferObject
{
    // Mandatory fields
    public string $amount;
    public string $invoiceNumber;
    public string $externalId;
    public string $dueDate;
    public string $issuedAt;
    public CustomerDTO $customer;
    /** @var \Capcito\InternalApiSdk\Models\InvoiceRowDTO[] */
    #[CastWith(InvoiceRowDTOArrayCaster::class)]
    public array $rows;

    // Optional
    public ?int $sellPrice;
    public ?int $id;
    public ?string $provider;
    public ?string $ocr;
    public ?float $balance;
    public ?float $creditableAmount;
    public ?float $vatAmount;
    public ?string $currency;
    public ?int $customerId;
    public ?TaxReductionDetailDTO $taxReductionDetails;
    public ?Carbon $paymentDate;
    /** @var \Capcito\InternalApiSdk\Models\PaymentDTO[] */
    #[CastWith(PaymentDTOArrayCaster::class)]
    public ?array $payments;
    /**
     * Obsolete, do not use
     */
    public ?bool $taxReduction;
    public ?string $status;
    public ?string $debtorReference;
    /**
     * Obsolete, do not use
     */
    public ?Carbon $issueDate;
    public ?Carbon $createdAt;
    public ?Carbon $updatedAt;
}
