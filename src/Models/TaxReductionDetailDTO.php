<?php

namespace Capcito\InternalApiSdk\Models;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class TaxReductionDetailDTO extends DataTransferObject
{
	public ?string $amount;
	public ?string $type;
}
