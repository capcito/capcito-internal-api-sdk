<?php

namespace Capcito\InternalApiSdk\Models;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class PaymentDTO extends DataTransferObject
{
	public float $amount;
	public string $currency;
	public string $paymentDate;
}
