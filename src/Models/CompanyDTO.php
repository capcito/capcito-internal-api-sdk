<?php

namespace Capcito\InternalApiSdk\Models;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;
use Capcito\InternalApiSdk\Models\BankAccountDTO;

#[Strict]
class CompanyDTO extends DataTransferObject
{
	public string $identityNumber;
	public string $name;
	public string $phone;
	public BankAccountDTO $bankAccounts;
}
