<?php

namespace Capcito\InternalApiSdk\Models;

use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class BankAccountDTO extends DataTransferObject
{
	public string $type;
	public string $number;
}
