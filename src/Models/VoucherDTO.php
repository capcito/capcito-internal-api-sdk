<?php

namespace Capcito\InternalApiSdk\Models;

use Carbon\Carbon;
use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;
use Capcito\InternalApiSdk\Casters\ToCarbonCaster;
use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Attributes\DefaultCast;


#[Strict]
#[DefaultCast(Carbon::class, ToCarbonCaster::class)]
class VoucherDTO extends DataTransferObject
{
    public ?int $id;
    public int $number;

    public ?Carbon $date;
    public ?string $serie;
    public ?int $fiscalYearId;
    public ?string $description;
    public ?Carbon $createdAt;
    public ?Carbon $updatedAt;

    #[CastWith(VoucherRowDTOArrayCaster::class)]
    public array $rows;
}
