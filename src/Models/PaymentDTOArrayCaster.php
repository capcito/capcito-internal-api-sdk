<?php

namespace Capcito\InternalApiSdk\Models;

use Capcito\InternalApiSdk\Models\InvoiceRowDTO;
use Exception;
use Spatie\DataTransferObject\Caster;

class PaymentDTOArrayCaster implements Caster
{
    public function cast(mixed $value): array
    {
        if (! is_array($value)) {
            throw new Exception("Can only cast arrays to PaymentDTO");
        }

        return array_map(
            fn (array $data) => new PaymentDTO(...$data),
            $value
        );
    }
}
