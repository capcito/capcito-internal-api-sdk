<?php

namespace Capcito\InternalApiSdk\Casters;

use Carbon\Carbon;
use Spatie\DataTransferObject\Caster;

class ToCarbonCaster implements Caster
{
    /**
     * @param array|mixed $value
     *
     * @return mixed
     */
    public function cast(mixed $value): Carbon
    {
        return Carbon::parse($value);
    }
}
