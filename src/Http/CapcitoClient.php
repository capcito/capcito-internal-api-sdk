<?php

namespace Capcito\InternalApiSdk\Http;

use Capcito\InternalApiSdk\Models\VoucherDTO;
use Exception;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\ClientInterface;
use Capcito\InternalApiSdk\Models\CompanyDTO;
use Capcito\InternalApiSdk\Models\InvoiceDTO;

class CapcitoClient
{
    const CREATE_INVOICE_ENDPOINT = 'company/customer-invoices';
    const UPDATE_INVOICE_ENDPOINT = 'company/customer-invoices';
    const UPDATE_COMPANY_ENDPOINT = 'user/companies/';

    const CREATE_VOUCHER_ENDPOINT = 'company/voucher';
    const UPDATE_VOUCHER_ENDPOINT = 'company/voucher';

    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function createInvoice(InvoiceDTO $invoice): InvoiceDTO
    {
        $response = $this->sendRequest('POST', self::CREATE_INVOICE_ENDPOINT, $invoice->toArray());

        return new InvoiceDTO(json_decode((string)$response->getBody(), true)['data']);
    }

    public function updateInvoice(InvoiceDTO $invoice): InvoiceDTO
    {
        $response = $this->sendRequest('PUT', self::UPDATE_INVOICE_ENDPOINT, $invoice->toArray());

        return new InvoiceDTO(json_decode((string) $response->getBody(), true)['data']);
    }

    public function updateCompany(CompanyDTO $company): CompanyDTO
    {
        if (!$company->id) {
            return 'ERROR: missing company ID.';
        }

        $path = self::UPDATE_COMPANY_ENDPOINT . $company->id;
        $response = $this->sendRequest('PUT', $path, $company->toArray());

        return new CompanyDTO(json_decode((string)$response->getBody(), true)['data']);
    }

    private function sendRequest(string $method, string $path, array $body): Response
    {
            // Strip null values since they might affect validation
            $body = array_filter($body);

            return $this->client->request($method, $path, [RequestOptions::JSON => $body]);
    }

    public function createVoucher(VoucherDTO $voucher)
    {
        $response = $this->sendRequest('POST', self::CREATE_VOUCHER_ENDPOINT, $voucher->toArray());

        return new VoucherDTO(json_decode((string)$response->getBody(), true)['data']);
    }

    public function updateVoucher(VoucherDTO $voucher)
    {
        $response = $this->sendRequest('POST', self::UPDATE_VOUCHER_ENDPOINT, $voucher->toArray());

        return new VoucherDTO(json_decode((string)$response->getBody(), true)['data']);
    }
}
